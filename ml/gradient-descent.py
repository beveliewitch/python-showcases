import math


def find_local_min(df, start_x, step_mul = 0.01, precision = 0.00001, max_iters = 15000):

    cur_x = start_x
    
    # max iteration for non-blocking algorithm
    for i in range(0, max_iters):
        prev_x = cur_x

        # multiplying the derivative point at current x with step multiplier constant, 
        # and apply to cur_x
        cur_x -= step_mul * df(prev_x)
        
        # compute difference between last x position and new x position
        step_size = abs(cur_x - prev_x)

    	# if step size is lower than precision we have found the minimum local
    	# because the compute point of derivative is near to 0
        if step_size <= precision:
            break
    
    return cur_x


# the derivative of sin(x²) is 2x * cos(x²)
df = lambda x : 2 * x * math.cos(x**2)

# compute local minimum for the function sin(x²) starting at position x=1.5
print("Local min : ", find_local_min(df, 1.5))
